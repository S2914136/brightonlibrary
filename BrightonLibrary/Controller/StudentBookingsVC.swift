//
//  StudentBookingsVC.swift
//  BrightonLibrary
//
//  Created by Uttam on 16/1/20.
//  Copyright © 2020 Uttam. All rights reserved.
//

import UIKit
import Firebase
import MessageUI

class StudentBookingsVC: UIViewController {
    @IBOutlet weak var bookingsearchbar: UISearchBar!
    
    @IBOutlet weak var searchFieldTxt: UITextField!
    @IBOutlet weak var studentlisttableview: UITableView!
    var studentBookingArray = [StudentBookings]()
    var timer: Timer?
    var myVariable: Int = 0

    var bookings = [String]()
    var choosenArray = [String]()
    var searching = false
    
    



  
   
    override func viewDidLoad() {
        super.viewDidLoad()
        searchFieldTxt.delegate = self
        searchFieldTxt.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)

//        DataService.instance.REF_BASE.observe(.value) { (snap) in
//              DataService.instance.getbookings { (studentBookings) in
//                      self.studentBookingArray = studentBookings
//                  }
//        }
       
          
      
        studentlisttableview.delegate = self
        studentlisttableview.dataSource = self
      
        //check()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        studentlisttableview.reloadData()
         DataService.instance.REF_BASE.observe(.value) { (snap) in
            DataService.instance.getbookings { (studentBookings) in
                self.studentBookingArray = studentBookings
                self.studentlisttableview.reloadData()
                
            }
         }
            
       
        
    }
    @objc func textFieldDidChange(){
//           if searchFieldTxt.text == ""{
//               bookings = []
//               studentlisttableview.reloadData()
//           }else{
//            DataService.instance.getName(forSearchedQuery: searchFieldTxt.text!) { (returnedname) in
//                self.bookings = returnedname
//                self.studentlisttableview.reloadData()
//            }
//           }
       }

  

}
extension StudentBookingsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  studentBookingArray.count
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
         let cell = studentlisttableview.dequeueReusableCell(withIdentifier: "studentbookingcell") as? StudentbookingCell
 
        
        let index = studentBookingArray[indexPath.row]
        cell?.studentEmail = index.studentEmail
        cell?.listID = index.id
        let del = UIContextualAction(style: .destructive, title:nil ){ (action, view, nil) in
            print("del")
            

        
            DataService.instance.REF_BOOKSCATAGORY.observeSingleEvent(of: .value) { (catasnap) in
                guard let catasnap = catasnap.children.allObjects as? [DataSnapshot] else {return}
                for snap in catasnap{
                    let key = snap.key
                    DataService.instance.REF_BOOKSCATAGORY.child(key).child("books").observeSingleEvent(of: .value) { (booksnap) in
                        guard let booksnap = booksnap.children.allObjects as? [DataSnapshot] else {return}
                        for book in booksnap{
                            let bookid = book.key
                            DataService.instance.REF_BOOKSCATAGORY.child(key).child("books").child(bookid).child("bookings").observeSingleEvent(of: .value) { (bookingsnap) in
                                guard let bookingsnap = bookingsnap.children.allObjects as? [DataSnapshot] else {return}
                                for booking in bookingsnap{
                                    if booking.key == index.id{
                                        DataService.instance.REF_BOOKSCATAGORY.child(key).child("books").child(bookid).child("bookings").child(booking.key).removeValue()
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }
                        DataService.instance.REF_BOOKINGS.observe(.value) { (snap) in
                            DataService.instance.REF_BOOKINGS.child(index.id).removeValue()
            
                            }
            
            
               
                self.studentlisttableview.beginUpdates()
               
                DataService.instance.REF_BOOKINGS.removeAllObservers()
                self.studentlisttableview.reloadData()
                self.studentlisttableview.endUpdates()
                
            
             self.studentBookingArray.remove(at: indexPath.row)
             self.studentlisttableview.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            
        }
        let mail = UIContextualAction(style: .normal, title:nil ){ (action, view, nil) in
            print("email")
            guard MFMailComposeViewController.canSendMail()else{
                      return
                  }
                  let composer = MFMailComposeViewController()
                  composer.mailComposeDelegate = self
                  composer.setSubject("Regarding fine on borrowed books")
            
                composer.setToRecipients([cell!.studentEmail!])
                  composer.setMessageBody("Fine", isHTML: true)
                    self.present(composer, animated: true)
        }
        let trash = UIImage(named: "trash")
        let mailimage = UIImage(named: "mail")
        //trash?.resize(maxWidthHeight: 25.0)
        
        
        del.image =  trash?.resize(maxWidthHeight: 40.0)
        del.backgroundColor = .darkGray
        mail.image = mailimage?.resize(maxWidthHeight: 45.0)
        mail.backgroundColor = .lightGray
        
        
        return UISwipeActionsConfiguration(actions: [del, mail])
    }
        
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = studentlisttableview.dequeueReusableCell(withIdentifier: "studentbookingcell") as? StudentbookingCell else {return UITableViewCell()}
        let index = studentBookingArray[indexPath.row]
//        if choosenArray.contains(bookings[indexPath.row]){
//            cell.configureCell(studentname: bookings[indexPath.row], fine: (index.fine ) , bookstakencount: index.bookTakendays.calendarTimeSinceNow(), contactNo: index.contactno, id: index.id, studentEmail: index.studentEmail, bookedDate: index.bookedDate)
//        }
            
                
                cell.configureCell(studentname: index.studentName, fine: (index.fine ) , bookstakencount: index.bookTakendays.calendarTimeSinceNow(), contactNo: index.contactno, id: index.id, studentEmail: index.studentEmail, bookedDate: index.bookedDate)
        
        
        
        
     return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
   
      
        
       // self.window?.rootViewController?.present(composer, animated: true, completion: nil)
       
        
    
    
}

extension UIImage {

    func resize(maxWidthHeight : Double)-> UIImage? {

        let actualHeight = Double(size.height)
        let actualWidth = Double(size.width)
        var maxWidth = 0.0
        var maxHeight = 0.0

        if actualWidth > actualHeight {
            maxWidth = maxWidthHeight
            let per = (100.0 * maxWidthHeight / actualWidth)
            maxHeight = (actualHeight * per) / 100.0
        }else{
            maxHeight = maxWidthHeight
            let per = (100.0 * maxWidthHeight / actualHeight)
            maxWidth = (actualWidth * per) / 100.0
        }

        let hasAlpha = true
        let scale: CGFloat = 0.0

        UIGraphicsBeginImageContextWithOptions(CGSize(width: maxWidth, height: maxHeight), !hasAlpha, scale)
        self.draw(in: CGRect(origin: .zero, size: CGSize(width: maxWidth, height: maxHeight)))

        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage
    }

}
extension StudentBookingsVC:MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if let _ = error{
            controller.dismiss(animated: true, completion: nil)
        }
        switch result {
        case .cancelled:
            print("cancelled")
        case .failed:
            print("cancelled")
        case .saved:
            print("saved")
        case .sent:
            print("sent")
        @unknown default:
            print("Error")
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
}
extension StudentBookingsVC: UITextFieldDelegate{
   
 
//    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
//           switch selectedScope {
//           case 0:
//                studentBookingArray = bookings
//           case 1:
//           studentBookingArray = bookings.filter({ student -> Bool in
//            student.studentName.lowercased().contains(bookingsearchbar.text!)
//           })
//
//           default:
//               break
//           }
//           studentlisttableview.reloadData()
//       }
  

    
}

