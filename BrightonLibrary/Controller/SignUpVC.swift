//
//  SignUpVC.swift
//  BrightonLibrary
//
//  Created by Uttam on 12/1/20.
//  Copyright © 2020 Uttam. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController {
    @IBOutlet weak var iDTxtF: UITextField!
    @IBOutlet weak var fullNameTxtF: UITextField!
    @IBOutlet weak var emailTxtF: UITextField!
    @IBOutlet weak var passwordTxtF: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func createAlert(title:String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (action) in
            
            alert.dismiss(animated: true, completion: nil)
           
        }))
        self.present(alert,animated: true , completion: nil)
         self.dismiss(animated: true, completion: nil)
    }
    

    
    @IBAction func signUpBtnpressed(_ sender: Any) {
        AuthService.instance.registerUser(withName: fullNameTxtF.text!, withID: iDTxtF.text!, withEmail: emailTxtF.text!, andPassword: passwordTxtF.text!, profilepic: " ") { (loginUsercreated, errorwhileCreatingUser) in
            if loginUsercreated{
                self.dismiss(animated: true, completion: nil)
                print("user is created")
                
            }else if !loginUsercreated{
                self.createAlert(title: "Could not Create and account ", message: (errorwhileCreatingUser!.localizedDescription))
            }
        }
    }
    

}
