//
//  AddBookCatagoryVC.swift
//  BrightonLibrary
//
//  Created by Uttam on 12/1/20.
//  Copyright © 2020 Uttam. All rights reserved.
//

import UIKit

class AddBookCatagoryVC: UIViewController {
    @IBOutlet weak var bookcatagoryNameTxt: UITextField!
    @IBOutlet weak var bookcatagoryTypeTxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func addbookCatagoryBtnpressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        DataService.instance.addbookCatagory(withBookCatagoryName: bookcatagoryNameTxt.text!, booktype: bookcatagoryTypeTxt.text!) { (bookcatagoryCreated) in
            if bookcatagoryCreated{
                print("book catagory added")
            }
        }
    }
}
