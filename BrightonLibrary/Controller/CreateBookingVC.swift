//
//  CreateBooking.swift
//  BrightonLibrary
//
//  Created by Uttam on 14/1/20.
//  Copyright © 2020 Uttam. All rights reserved.
//

import UIKit

class CreateBookingVC: UIViewController {

    @IBOutlet weak var contactNoTxt: UITextField!
    @IBOutlet weak var studentEmailTxt: UITextField!
    @IBOutlet weak var studentFullNameTxt: UITextField!
    @IBOutlet weak var studentID: UITextField!
    var bookID:String?
    var bookcatagoryID: String?
    var catagoryname: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        print(bookID!)
        print(bookcatagoryID!)
        
        // Do any additional setup after loading the view.
    }
    
    func updatebooks(){
        DataService.instance.REF_BOOKINGS.observe(.value) { (snap) in
            
            DataService.instance.REF_BOOKSCATAGORY.child(self.bookcatagoryID!).child("books").child(self.bookID!).child("bookings").observe(.value) { (childnumber) in
                let bookingsnumber = childnumber.childrenCount
                let bookingnumberInt = Int(bookingsnumber)
                print(bookingsnumber)
                let availref = DataService.instance.REF_BOOKSCATAGORY.child(self.bookcatagoryID!).child("books").child(self.bookID!)
                let availablebook = DataService.instance.REF_BOOKSCATAGORY.child(self.bookcatagoryID!).child("books").child(self.bookID!).child("total_no_oFBooks")
                availablebook.observeSingleEvent(of: .value) { (no_snap) in
                    availablebook.removeAllObservers()
                    let number = no_snap.value as! Int
                    print("The number is\(number - bookingnumberInt)")
                    let totalnumber = number - bookingnumberInt
                    if bookingnumberInt != 0{
                        availref.updateChildValues(["available": totalnumber])
                    }else{
                        print("No book available")
                    }
                }
            }
        }
    }
 
    @IBAction func createBookingNowBtnPressed(_ sender: Any) {
        let dateforRecord = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let datetoSave = formatter.string(from: dateforRecord)
        let key =  DataService.instance.REF_BOOKINGS.childByAutoId().key
        DataService.instance.REF_BOOKINGS.observe(.value) { (snap) in
            
            DataService.instance.REF_BOOKSCATAGORY.child(self.bookcatagoryID!).child("books").child(self.bookID!).child("bookings").child(key!).updateChildValues(["bookedBy": self.studentFullNameTxt.text!, "studentEmail": self.studentEmailTxt.text!, "studentNo": self.studentID.text!, "fine": 0,"type":self.catagoryname!,"contactNo":self.contactNoTxt.text!,"bookingID": key! ,"createdAt": [".sv": "timestamp"], "bookedDay": datetoSave])
            
            
        }
        self.updatebooks()
        
        
        DataService.instance.REF_BOOKINGS.child(key!).updateChildValues(["bookedBy": self.studentFullNameTxt.text!, "studentEmail": self.studentEmailTxt.text!, "studentNo": self.studentID.text!, "bookingID": key!, "fine": 0,"type":self.catagoryname!,"contactNo":contactNoTxt.text!,"createdAt": [".sv": "timestamp"],"bookedDay": datetoSave])
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
