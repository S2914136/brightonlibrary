//
//  BooksVC.swift
//  BrightonLibrary
//
//  Created by Uttam on 13/1/20.
//  Copyright © 2020 Uttam. All rights reserved.
//

import UIKit
import Firebase

class BooksVC: UIViewController {

    @IBOutlet weak var bookTableview: UITableView!
    var catagoryID = String()
    var catagoryname = String()
    var booksArray = [Books]()
    
    @IBOutlet weak var booksLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        bookTableview.delegate = self
        bookTableview.dataSource = self

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        booksLbl.text! = "\(catagoryname) Books"
        DataService.instance.REF_BOOKSCATAGORY.observe(.value) { (snap) in
            self.downloadBooksFromCatagory { (booksnap) in
                self.booksArray = booksnap
            }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "toAddBook"{
               
                   let catagoryList = segue.destination as! AddBookVC
                   
                catagoryList.catagoryID = catagoryID
                catagoryList.bookcatagory = catagoryname
            
               
           }else if segue.identifier == "tocreatebooking"{
            if let indexPath = bookTableview.indexPathForSelectedRow{
                          let creatingbookingVC = segue.destination as! CreateBookingVC
                          let book = booksArray[indexPath.row]
                creatingbookingVC.bookID = book.catagoryID
                creatingbookingVC.bookcatagoryID = catagoryID
                creatingbookingVC.catagoryname = catagoryname
                
                
                
                
                         
                      }
        }
    }
    func downloadBooksFromCatagory(handler: @escaping(_ book:[Books])->()){
        var booksArray = [Books]()
        DataService.instance.REF_BOOKSCATAGORY.observe(.value) { (snap) in
           DataService.instance.REF_BOOKSCATAGORY.child(self.catagoryID).child("books").observeSingleEvent(of: .value) { (bookssnap) in
                guard let bookssnap = bookssnap.children.allObjects as? [DataSnapshot] else{return}
                for book in bookssnap{
                    let available = book.childSnapshot(forPath: "available").value as! Int
                    let bookID = book.childSnapshot(forPath: "bookID").value as! String
                    let bookname = book.childSnapshot(forPath: "bookName").value as! String
                    let manufacturer = book.childSnapshot(forPath: "manufacturer").value as! String
                    let total = book.childSnapshot(forPath: "total_no_oFBooks").value as! Int 
                    let book = Books(bookName: bookname, available: available, catagoryID: bookID, total: total, manufacturer: manufacturer)
                    booksArray.append(book)
                }
                handler(booksArray)
                self.bookTableview.reloadData()
            }
        }
    }
    
}
extension BooksVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        booksArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = bookTableview.dequeueReusableCell(withIdentifier: "bookcell") as? BooksCell else {return UITableViewCell()}
        let books = booksArray[indexPath.row]
        cell.configureCell(bookname: books.bookName, total: books.total, available: books.available, manufacturername: books.manufacturer, bookID: books.catagoryID)
        return cell
    }
    
    
    
}
