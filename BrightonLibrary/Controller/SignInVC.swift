//
//  ViewController.swift
//  BrightonLibrary
//
//  Created by Uttam on 11/1/20.
//  Copyright © 2020 Uttam. All rights reserved.
//

import UIKit

class SignInVC: UIViewController {

    @IBOutlet weak var emailTxtF: UITextField!
    @IBOutlet weak var passwordTxtF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordTxtF.isSecureTextEntry = true
        // Do any additional setup after loading the view.
    }
    func createAlert(title:String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert,animated: true , completion: nil)
    }

    @IBAction func signInBtnPressed(_ sender: Any) {
        AuthService.instance.loginUser(withID: emailTxtF.text!, andPassword: passwordTxtF.text!) { (userLoggedin, loginError) in
            if userLoggedin{
                let tabVC = self.storyboard?.instantiateViewController(identifier: "tabVC")
                self.present(tabVC!, animated: true, completion: nil)
            }else if loginError != nil{
                self.createAlert(title: "Login error", message: (loginError?.localizedDescription)!)
            }
        }
    }
    
}

