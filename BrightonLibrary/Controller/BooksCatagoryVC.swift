//
//  BooksVC.swift
//  BrightonLibrary
//
//  Created by Uttam on 12/1/20.
//  Copyright © 2020 Uttam. All rights reserved.
//

import UIKit

class BooksCatagoryVC: UIViewController {
    @IBOutlet weak var BooksCatagoryTableview: UITableView!
    var catagoryArray = [BookCatagories]()
    override func viewDidLoad() {
        super.viewDidLoad()
       
//        DataService.instance.getbookCatagories { (bookcatagories) in
//            self.catagoryArray = bookcatagories
//            self.BooksCatagoryTableview.reloadData()
//        }
        BooksCatagoryTableview.delegate = self
        BooksCatagoryTableview.dataSource = self

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        DataService.instance.REF_BOOKSCATAGORY.observe(.value) { (snap) in
            DataService.instance.getbookCatagories { (bookcatagories) in
                self.catagoryArray = bookcatagories
                self.BooksCatagoryTableview.reloadData()
            }
        }
       
        
    }
   

}
extension BooksCatagoryVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        catagoryArray.count
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
           return true
       }
       func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
            let cell = BooksCatagoryTableview.dequeueReusableCell(withIdentifier: "bookcatagorycell") as? BookcatagoryCellTableViewCell
    
           
        let index = catagoryArray[indexPath.row]
        cell?.catagoryID = index.catagoryID
     
           let del = UIContextualAction(style: .destructive, title:nil ){ (action, view, nil) in
               print("del")
               
               DataService.instance.REF_BOOKSCATAGORY.observe(.value) { (snap) in
                DataService.instance.REF_BOOKSCATAGORY.child(index.catagoryID).removeValue()
                   
                  
                   self.BooksCatagoryTableview.beginUpdates()
                  
                   DataService.instance.REF_BOOKINGS.removeAllObservers()
                   self.BooksCatagoryTableview.reloadData()
                   self.BooksCatagoryTableview.endUpdates()
                   
               }
                self.catagoryArray.remove(at: indexPath.row)
                self.BooksCatagoryTableview.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
               
           }
           
           let trash = UIImage(named: "trash")
    
           //trash?.resize(maxWidthHeight: 25.0)
           
           
           del.image =  trash?.resize(maxWidthHeight: 40.0)
           del.backgroundColor = .darkGray
       
           
           
           return UISwipeActionsConfiguration(actions: [del])
       }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = BooksCatagoryTableview.dequeueReusableCell(withIdentifier: "bookcatagorycell") as? BookcatagoryCellTableViewCell else {return UITableViewCell()}
        let catagory = catagoryArray[indexPath.row]
        cell.configureCell(catagoryName: catagory.bookcatagoryName, catagoryID: catagory.catagoryID)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toBooksVC"{
            if let indexpath = BooksCatagoryTableview.indexPathForSelectedRow{
                let catagoryList = segue.destination as! BooksVC
                let catagories = catagoryArray[indexpath.row]
                catagoryList.catagoryID = catagories.catagoryID
                catagoryList.catagoryname = catagories.bookcatagoryName
                
            }
        }
    }


}
