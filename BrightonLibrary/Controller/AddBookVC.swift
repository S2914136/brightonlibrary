//
//  CreateVC.swift
//  BrightonLibrary
//
//  Created by Uttam on 13/1/20.
//  Copyright © 2020 Uttam. All rights reserved.
//

import UIKit
import Firebase

class AddBookVC: UIViewController {
    var catagoryID = String()
    var bookcatagory = String()

    @IBOutlet weak var year_EditonTxt: UITextField!
    @IBOutlet weak var bookNameText: UITextField!
    @IBOutlet weak var no_ofAvailabilityTxt: UITextField!
    @IBOutlet weak var manufacturedByTxt: UITextField!
    @IBOutlet weak var totalBooksTxt: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    

    @IBAction func addBookBtnPressed(_ sender: Any) {
        let autoID = DataService.instance.REF_BOOKSCATAGORY.childByAutoId().key
        DataService.instance.REF_BOOKSCATAGORY.child(catagoryID).child("books").child(autoID!).updateChildValues(["bookID": autoID!, "bookName": bookNameText.text!, "available": Int(no_ofAvailabilityTxt.text!)!, "manufacturer": manufacturedByTxt.text!, "total_no_oFBooks": Int(totalBooksTxt.text!)! ])
        self.dismiss(animated: true, completion: nil)
    }
    
}
