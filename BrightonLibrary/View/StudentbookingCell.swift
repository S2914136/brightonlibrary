//
//  StudentbookingCell.swift
//  BrightonLibrary
//
//  Created by Uttam on 16/1/20.
//  Copyright © 2020 Uttam. All rights reserved.
//

import UIKit
import Firebase
import MessageUI

class StudentbookingCell: UITableViewCell {
    @IBOutlet weak var studentname: UILabel!
    @IBOutlet weak var fine: UILabel!
    @IBOutlet weak var bookedDayscount: UILabel!
    @IBOutlet weak var contactNo: UILabel!
    var listID: String?
    @IBOutlet weak var emailBtn: UIButton!
    var studentEmail: String?
    var index:Int?
    var bookedDate:String?
    //var timer: Timer?
    var myVariable: Int = 0
    
    //var timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
    override func awakeFromNib() {
        super.awakeFromNib()
       
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
//    @objc func runTimedCode(){
//        DataService.instance.REF_BOOKINGS.queryOrderedByKey().observeSingleEvent(of: .value) { (booksnap) in
//            guard let booksnap = booksnap.children.allObjects as? [DataSnapshot] else{return}
//            let id = booksnap[self.index!]
//            let key = id.key
//            let fineref =  DataService.instance.REF_BOOKINGS.child(key)
//            fineref.observe(.value) { (snap) in
//                let fine = snap.value as? Int
//
//                if self.bookedDayscount.text! == "3 hours ago"{
//                    fineref.updateChildValues(["fine": addfine ])
//                }
//                fineref.removeAllObservers()
//            }
//        }
//           myVariable += 2
//
//
//       }
    func configureCell(studentname: String, fine: Int, bookstakencount: String, contactNo: String, id: String, studentEmail:String, bookedDate: String){
        emailBtn.layer.cornerRadius = 10
        self.studentname.text! = studentname
        self.fine.text! = "\(fine)"
        self.bookedDayscount.text! = bookstakencount
        self.contactNo.text! = contactNo
        self.listID = id
        self.studentEmail = studentEmail
        self.bookedDate = bookedDate
        
       
            let currentDate = Date()
            let formatter = DateFormatter()
            
            formatter.dateFormat = "dd.MM.yyyy"
            
            if let endDate = formatter.date(from:bookedDate) {
                let components = Calendar.current.dateComponents([.day], from:endDate , to: currentDate)
                let days = components.day
                if days! >= 16{
                    self.fine.text! = "$\((days! - 15)*2)"
                    
                }
       
        }
       
    }
   
    @IBAction func EmailBtnPressed(_ sender: Any) {
       
        //showMailComposer()
    }
    
//    func showMailComposer(){
//        guard MFMailComposeViewController.canSendMail()else{
//            return
//        }
//        let composer = MFMailComposeViewController()
//        composer.mailComposeDelegate = self
//        composer.setSubject("Regarding fine on borrowed books")
//        composer.setToRecipients(["Itsupport@brighton.edu.au"])
//        composer.setMessageBody("Fine", isHTML: true)
//
//       // self.window?.rootViewController?.present(composer, animated: true, completion: nil)
//
//
//    }
}
extension StudentbookingCell: MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if let _ = error{
            controller.dismiss(animated: true, completion: nil)
        }
        switch result {
        case .cancelled:
            print("cancelled")
        case .failed:
            print("cancelled")
        case .saved:
            print("saved")
        case .sent:
            print("sent")
        @unknown default:
            print("Error")
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
}
