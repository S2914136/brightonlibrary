//
//  BooksCell.swift
//  BrightonLibrary
//
//  Created by Uttam on 13/1/20.
//  Copyright © 2020 Uttam. All rights reserved.
//

import UIKit

class BooksCell: UITableViewCell {
    @IBOutlet weak var bookNameLbl: UILabel!
    @IBOutlet weak var totalBooksLbl: UILabel!
    @IBOutlet weak var availableBooksLbl: UILabel!
    @IBOutlet weak var manufacturerNameLbl: UILabel!
    var bookID:String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
        
        // Initialization code
    }
 

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(bookname:String, total:Int, available:Int, manufacturername:String, bookID:String){
        self.bookNameLbl.text! = bookname
        self.totalBooksLbl.text! = "\(total)"
        self.availableBooksLbl.text! = "\(available)"
        self.manufacturerNameLbl.text! = manufacturername
        self.bookID = bookID
        
    }
  
    
    
}
