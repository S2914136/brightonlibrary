//
//  BookcatagoryCellTableViewCell.swift
//  BrightonLibrary
//
//  Created by Uttam on 12/1/20.
//  Copyright © 2020 Uttam. All rights reserved.
//

import UIKit

class BookcatagoryCellTableViewCell: UITableViewCell {
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var catagoryName: UILabel!
    var catagoryID:String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    func configureCell( catagoryName: String, catagoryID:String){
        self.bgImage.image! = UIImage(named: "BookingView")!
        self.catagoryName.text! = catagoryName
        self.catagoryID = catagoryID
    }

}
