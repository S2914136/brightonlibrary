//
//  Books.swift
//  BrightonLibrary
//
//  Created by Uttam on 14/1/20.
//  Copyright © 2020 Uttam. All rights reserved.
//

import Foundation
class Books{
        private var _bookName: String
        private var _available: Int
        private var _catagoryID: String
        private var _total: Int
        private var _manufacturer: String
        
        //private var _key: String
        
        
        var bookName :String{
            return _bookName
        }
        var available :Int{
            return _available
        }
        var total :Int{
            return _total
        }
        var catagoryID :String{
            return _catagoryID
        }
        var manufacturer :String{
            return _manufacturer
        }
        
    init(bookName:String, available:Int, catagoryID:String, total: Int, manufacturer: String){
            self._bookName = bookName
            self._available = available
            self._catagoryID = catagoryID
            self._total = total
            self._manufacturer = manufacturer
        }
    
}
