//
//  BookCatagories.swift
//  BrightonLibrary
//
//  Created by Uttam on 12/1/20.
//  Copyright © 2020 Uttam. All rights reserved.
//

import Foundation
class BookCatagories{
    private var _bookcatagoryName: String
    private var _bookcatagoryType: String
    private var _catagoryID: String
    
    //private var _key: String
    
    
    var bookcatagoryName :String{
        return _bookcatagoryName
    }
    var bookcatagoryType :String{
        return _bookcatagoryType
    }
    var catagoryID :String{
        return _catagoryID
    }
    
    init(bookcatagoryName:String, bookcatagoryType:String, catagoryID:String){
        self._bookcatagoryName = bookcatagoryName
        self._bookcatagoryType = bookcatagoryType
        self._catagoryID = catagoryID
        //self._key = key
    }
}
