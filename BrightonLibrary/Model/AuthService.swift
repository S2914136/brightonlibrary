//
//  AuthService.swift
//  BrightonLibrary
//
//  Created by Uttam on 12/1/20.
//  Copyright © 2020 Uttam. All rights reserved.
//

import Foundation
//
//  AuthService.swift
//  Brighton College App
//
//  Created by Uttam on 8/11/18.
//  Copyright © 2018 Developers. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth
import FirebaseCore
import FirebaseDatabase

class AuthService {
    static let instance = AuthService()
    
    func registerUser(withName fullName:String, withID ID: String, withEmail email: String, andPassword password: String, profilepic: String,userCreationComplete: @escaping(_ status:Bool, _ error:Error?) -> ()){
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            guard let user = user?.user else{
                userCreationComplete(false, error)
                return
            }
            //let users = user.user
            let userData = ["name": fullName,"provider": user.providerID, "email": user.email, "SID": ID, "id": user.uid]
            DataService.instance.createDBUser(uid: user.uid, userData: userData as Any as! Dictionary<String, Any>)
            userCreationComplete(true,nil)
        }
       
    }
    
   
  


    func loginUser( withID studentandStaffID: String,  andPassword password: String, loginComplete: @escaping(_ status:Bool, _ error:Error?) -> ()){
        Auth.auth().signIn(withEmail: studentandStaffID, password: password) { (user, error) in
            if error != nil{
                loginComplete(false, error)
                return
            }
            loginComplete(true,nil)
        }
        
    }


    

}
