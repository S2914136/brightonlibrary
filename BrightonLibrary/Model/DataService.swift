//
//  Dataservice.swift
//  BrightonLibrary
//
//  Created by Uttam on 12/1/20.
//  Copyright © 2020 Uttam. All rights reserved.
//

import Foundation
import Firebase

let DB_BASE = Database.database().reference()
class DataService{
    static let instance = DataService()
      var index:Int?
     
     private var _REF_BASE = DB_BASE
     private var _REF_USERS = DB_BASE.child("users")
     private var _REF_BOOKSCATAGORY = DB_BASE.child("bookcatagories")
     private var _REF_BOOKINGS = DB_BASE.child("bookings")
   

    
     var REF_BASE: DatabaseReference{
         return _REF_BASE
     }
     var REF_USERS: DatabaseReference{
         return _REF_USERS
     }
     var REF_BOOKSCATAGORY: DatabaseReference{
         return _REF_BOOKSCATAGORY
     }
     var REF_BOOKINGS: DatabaseReference{
         return _REF_BOOKINGS
     }
     
    func createDBUser(uid: String, userData:Dictionary<String,Any>){
           REF_USERS.child(uid).updateChildValues(userData)
       }
    func addbookCatagory(withBookCatagoryName catagoryname: String, booktype type: String, bookcreation: @escaping (_ status: Bool)->()){
        let key = REF_BOOKSCATAGORY.childByAutoId().key
        let userid = Auth.auth().currentUser?.uid
        self.REF_BOOKSCATAGORY.child(key!).updateChildValues(["catagorykey": key!, "createdby": userid!, "catagoryname": catagoryname, "booktype": type])
    }
    func getbookCatagories(handler: @escaping(_ bookcatagories: [BookCatagories])->()){
        var bookcatagoriesArray = [BookCatagories]()
        REF_BOOKSCATAGORY.queryOrderedByKey().observeSingleEvent(of: .value) { (cataSnap) in
            guard let catasnap = cataSnap.children.allObjects as? [DataSnapshot] else {return}
            for catagories in  catasnap{
                let cataname = catagories.childSnapshot(forPath: "catagoryname").value as! String
                let cataid = catagories.childSnapshot(forPath: "catagorykey").value as! String
                let bookcatagory = BookCatagories(bookcatagoryName: cataname, bookcatagoryType: " ", catagoryID: cataid)
                bookcatagoriesArray.append(bookcatagory)
                
            }
            handler(bookcatagoriesArray)
        }
    }
    func getbookings(handler: @escaping(_ bookcatagories: [StudentBookings])->()){
        var studentsArray = [StudentBookings]()
        REF_BOOKINGS.queryOrderedByKey().observeSingleEvent(of: .value) { (bookingSnap) in
            guard let bookingsnap = bookingSnap.children.allObjects as? [DataSnapshot] else {return}
            
           
                
            for booking in  bookingsnap{
                let studentname = booking.childSnapshot(forPath: "bookedBy").value as! String
                let bookingdate = booking.childSnapshot(forPath: "createdAt").value as! Double
                let bookingid = booking.childSnapshot(forPath: "bookingID").value as! String
                let studentNo = booking.childSnapshot(forPath: "studentNo").value as! String
                let studentemail = booking.childSnapshot(forPath: "studentEmail").value as! String
                let studentfine = booking.childSnapshot(forPath: "fine").value as! Int
                let bookedDate = booking.childSnapshot(forPath: "bookedDay").value as! String
                let studentBooking = StudentBookings(studentName: studentname, fine: studentfine, bookTakendays: bookingdate, id: bookingid, contactno: studentNo, studentEmail: studentemail, bookedDate: bookedDate)
                studentsArray.append(studentBooking)
          
                
            }
            handler(studentsArray)
        }
    }
    
    func getStudentUserName(forUID bookedby: [String], handler: @escaping(_ username: [String])->()){
        var idArray = [String]()
          REF_BOOKINGS.observeSingleEvent(of: .value) { (studentNameSnap) in
              guard let studentNameSnap = studentNameSnap.children.allObjects as? [DataSnapshot] else {return}
              for student in studentNameSnap{
                  let name =  student.childSnapshot(forPath: "name").value as! String
                                if bookedby.contains(name){
                                  
                                    idArray.append(student.key)
                }
            }
                  
              handler(idArray)
          }
      }
    func getName(forSearchedQuery query: String, handler: @escaping (_ nameArray: [String])-> ()){
         var nameArray = [String]()
         REF_BOOKSCATAGORY.observe(.value) { (studentSnapshot) in
             guard let studentsnapshot = studentSnapshot.children.allObjects as? [DataSnapshot] else{return}
             for student in studentsnapshot{
                 let name =  student.childSnapshot(forPath: "bookedBy").value as! String
                 if name.contains(query) == true /*&& name != Auth.auth().currentUser?.displayName*/{
                     nameArray.append(name)
                 }
             }
             handler(nameArray)
         }
     }
    
}
