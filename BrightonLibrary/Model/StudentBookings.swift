//
//  StudentBookings.swift
//  BrightonLibrary
//
//  Created by Uttam on 15/1/20.
//  Copyright © 2020 Uttam. All rights reserved.
//

import Foundation
class StudentBookings{
        private var _studentName: String
        private var _fine: Int
        private var _contactno: String
        private var _bookTakendays: Date
        private var _id: String
        private var _studentEmail: String
        private var _bookedDate: String
        
        var studentName:String{
            return _studentName
        }
        var fine:Int{
            return _fine
        }
        var contactno: String{
            return _contactno
        }
        var bookTakendays: Date{
            return _bookTakendays
       }
        var id: String{
            return _id
        }
        var bookedDate: String{
                return _bookedDate
        }
    
        var studentEmail:String{
            return _studentEmail
        }
    init(studentName: String, fine: Int, bookTakendays:Double, id: String, contactno:String, studentEmail: String, bookedDate: String){
            self._studentName = studentName
            self._fine = fine
            self._bookTakendays = Date(timeIntervalSince1970: bookTakendays / 1000)
            self._contactno = contactno
            self._id = id
            self._studentEmail = studentEmail
            self._bookedDate = bookedDate
        }
    
    
    
}
